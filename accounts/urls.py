from django.urls import path

from . import views

urlpatterns = [
    path('discord/', views.discord_redirect),
    path('login', views.login),
    path('sc2', views.sc2login),
    path('sc2_redirect', views.sc2_callback)

]
