from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.conf import settings
from requests_oauthlib import OAuth2Session
# Create your views here.

def discord_token_updater(request, token):
    request.session['discord_oauth2_token'] = token

def sc2_token_updater(request, token):
    request.session['sc2_oauth2_token'] = token

def oath_session(request, token=None, state=None, scope=None, updater=None, **kwargs):
    updater = updater if updater else lambda token: discord_token_updater(request, token)
    return OAuth2Session(
        client_id=settings.DISCORD_CLIENT_ID,
        token=token,
        state=state,
        redirect_uri=settings.DISCORD_REDIRECT_URI,
        scope=scope or 'identify',
        auto_refresh_kwargs= {
            'client_id': settings.DISCORD_CLIENT_ID,
            'client_secret': settings.DISCORD_CLIENT_SECRET
        },
        auto_refresh_url=settings.DISCORD_TOKEN_URL,
        token_updater=lambda token: discord_token_updater(request, token),
        **kwargs
    )

def sc2_oath_session(request, token = None, state = None, scope = None, updater = None, ** kwargs):
    return OAuth2Session(
        client_id=settings.BNET_CLIENT_ID,
        token=token,
        state=state,
        redirect_uri=settings.BNET_REDIRECT_URI,
        scope=scope or 'sc2.profile',
        auto_refresh_kwargs={
            'client_id': settings.BNET_CLIENT_ID,
            'client_secret': settings.BNET_CLIENT_SECRET
        },
        auto_refresh_url=settings.DISCORD_TOKEN_URL,
        token_updater=lambda token: sc2_token_updater(request, token),
        **kwargs
    )

def login(request):
    discord = oath_session(request)
    auth_url, state = discord.authorization_url(settings.DISCORD_AUTHORIZATION_BASE_URL)
    request.session['discord_oauth2_state'] = state
    return HttpResponseRedirect(auth_url)

def discord_redirect(request):
    response_uri = 'https://localhost:8000/'
    dstate = request.session.get('discord_oauth2_state')
    discord = oath_session(request, state=dstate)
    token = discord.fetch_token(
        settings.DISCORD_TOKEN_URL,
        client_secret=settings.DISCORD_CLIENT_SECRET,
        code=request.GET.get('code'),
        authorization_response=None
    )
    request.session['discord_oauth2_token'] = token
    discord = oath_session(request, token=token)
    print(discord.get(settings.DISCORD_API_BASE_URL + '/users/@me').json())
    return HttpResponse('Yay')


def sc2login(request):
    sc2 = sc2_oath_session(request)
    auth_url, state = sc2.authorization_url(settings.BNET_AUTHORIZATION_BASE_URL)
    print(auth_url)
    request.session['bnet_oauth2_state'] = state
    return HttpResponseRedirect(auth_url)

def sc2_callback(request):
    print("in callback")
    updater = lambda t: discord_token_updater(request, t)
    state = request.session.get('bnet_oauth2_state')
    sc2 = sc2_oath_session(request, state=state)
    token = sc2.fetch_token(
        settings.BNET_TOKEN_URL,
        scope='sc2.profile',
        client_secret=settings.BNET_CLIENT_SECRET,
        code=request.GET.get('code'),
        authorization_response=None
    )
    request.session['bnet_oauth2_token'] = token
    sc2 = sc2_oath_session(request, token=token)

    account_id = sc2.get(settings.BNET_API_BASE_URL + '/oauth/userinfo').json().get('id')
    req_uri = settings.BNET_API_REQUEST_URL + f"/sc2/player/{account_id}"
    print(req_uri)
    print(sc2.get(req_uri).json())
    return HttpResponse("Yay")