from accounts.models import DiscordUser
class DiscordPlayerSerializer:
    class Meta:
        model = DiscordUser
        